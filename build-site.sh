#!/usr/bin/env bash

npx asciidoctor-revealjs --destination-dir public/hjelp --out-file index.html public/hjelp/hjelp.adoc
sed -i '' 's;node_modules/reveal.js/dist;../reveal.js;g;' public/hjelp/index.html

npx asciidoctor-revealjs --destination-dir public/trening/1 --out-file index.html public/trening/1/trening-1.adoc
sed -i '' 's;node_modules/reveal.js/dist;../../reveal.js;g;' public/trening/1/index.html
